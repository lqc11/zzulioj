#include <stdio.h>
#include <math.h>

int main()
{
	double x,y,ans;
	char op;
	int error = 0; 
	scanf("%lf %c %lf", &x, &op, &y);
	switch(op)
	{
	case '+':ans = x + y;break;
	case '-':ans = x - y;break;
	case '*':ans = x * y;break;
	case '/':
	        if(fabs(y) < 1e-10){
	        	error = 1;
			}
			else{
				ans = x / y;
			}break;
	default: error = 1;
    }
	if(error > 0){
		printf("Wrong input!");
	}else{
		printf("%.2lf",ans);
	}
	return 0;		
}               
