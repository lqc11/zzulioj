#include<stdio.h>

int main()
{
    int n, temp, base;
    scanf("%d", &n);
    temp = n;
    base = 1;
    while( temp > 9) 
    {
        temp /= 10; 
        base *= 10; 
    }
    while(base > 0) 
    {
        printf("%d ", n/base); 
        n %= base;
        base /= 10;
    }
    printf("\n");
    return 0;
}
