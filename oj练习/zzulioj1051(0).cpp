#include<stdio.h>
#include<math.h>
int main()
{
	int item, n;
	scanf("%d %d", &item, &n);
	double sum = 0;
	double t = item;
	for(int i = 1; i <= n; i++)
	{
		sum += t; 
		t = sqrt(t);
	}
	printf("%.2lf\n", sum);
	return 0;
}
