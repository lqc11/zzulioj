#include<stdio.h>

int main()
{
    int i;
    double x, flag, sum, deno, numerator;

    scanf("%lf",  &x);

    sum = 0.0;
    deno = 1.0; //第一项的分母
    numerator = x; //第一项的分子
    flag = 1.0;  //第一项的符号

    for(i = 1; i <= 10; i++)
    {
        sum += numerator * flag / deno;

	//为下一项的计算做准备
        flag = -flag;
        numerator = numerator * x * x;
        deno =  deno *(2 * i) * ( 2 * i + 1);
    }

    printf("%.3f\n", sum);
    return 0;
}
